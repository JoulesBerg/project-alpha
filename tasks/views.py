from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView
from tasks.models import Task

# Create your views here.


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    fields = [
        "name",
        "start_date",
        "due_date",
        "project",
        "assignee",
    ]
    template_name = "tasks/taskcreate.html"

    def form_valid(self, form):
        new = form.save(commit=False)
        new.save()
        return redirect("show_project", pk=new.project.id)


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    context_object_name = "tasks"
    template_name = "tasks/tasklist.html"

    def get_queryset(self):
        user = self.request.user
        return Task.objects.filter(assignee=user)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
