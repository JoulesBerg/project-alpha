from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView, CreateView

from projects.models import Project

# Create your views here.


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    context_object_name = "projects"
    template_name = "projects/projectlist.html"

    def get_queryset(self):
        user = self.request.user
        return Project.objects.filter(members=user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    context_object_name = "project"
    template_name = "projects/projectdetail.html"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/projectcreate.html"
    fields = [
        "name",
        "description",
        "members",
    ]

    def form_valid(self, form):
        new = form.save(commit=False)
        new.save()
        return redirect("show_project", pk=new.id)
